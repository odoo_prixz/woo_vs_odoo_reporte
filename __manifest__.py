{
    'name': 'Odoo vs Woo stock',
    'version': '14.0.1.0.0',
    'category': 'stock',
    'summary': """Odoo vs Woo stock""",
    'description': """
        obtiene el reporte de odoo vs Woo en stocks
    """,
    'author': 'Prixz',
    'depends': ['woo_multi_warehouse'],
    'data': [
        'security/ir.model.access.csv',
        'data/ir_cron.xml',
        'views/woo_odoo_report_stock_views.xml',
    ],
    'license': "LGPL-3",
    'installable': True,
    'application': False,
}
