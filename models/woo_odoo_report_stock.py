from odoo import fields, models
from woocommerce import API
import logging
import json

_logger = logging.getLogger(__name__)


class WooOdooReportStock(models.Model):
    _name = 'woo_odoo.report_stock'
    _rec_name = 'store_product_id'

    product_id = fields.Many2one("product.product", string="Producto Odoo", )
    store_product_id = fields.Char("Store Template ID", )
    cantidad_woo = fields.Float(string="Cantidad en Woo", )
    cantidad_odoo = fields.Float(string="Cantidad en Odoo", )
    diferencia = fields.Float(string="Diferencia", )
    state = fields.Selection([
        ('equal', 'Igual'),
        ('positive', 'positivo'),
        ('negative', 'negativo'),
        ('updated', 'Actualizado')], string='Estado')
    warehouse_id = fields.Many2one(
        'stock.warehouse', string='Almacén')
    stock_total_woo = fields.Float(string="Cantidad en total woo", group_operator=False)

    def get_qty_meta(self,meta_data, warehouse_id):
        for meta in meta_data:
            if meta["key"] == warehouse_id.woo_branch:
                try:
                    stock = float(meta["value"])
                except Exception as e:
                    return 0
                return stock
        return 0

    def _cron_get_odoo_vs_woo_stock(self):
        connection = self._get_woocommerce_connection()
        if not connection:
            return
        warehouse_ids = self.env["stock.warehouse"].search([
            ("is_multi_stock_woo_activate", "=", True),
        ])
        if not warehouse_ids:
            return
        products_mapping_ids = self.env["channel.product.mappings"].search([])
        woo_report = self.env['woo_odoo.report_stock']
        stock_quant = self.env['stock.quant']
        for mapping in products_mapping_ids:
            url = f'products/{mapping.store_product_id}'
            request_post = connection.get(url)
            if request_post.ok:
                product = json.loads(request_post.content)
                stock_total_woo = product.get("stock_quantity") if product.get("stock_quantity") else 0
                product_odoo = mapping.product_name
                meta_data = product.get("meta_data")
                for warehouse_id in warehouse_ids:
                    stock_woo = float(self.get_qty_meta(meta_data, warehouse_id))
                    stock_odoo = stock_quant._get_available_quantity(
                        product_odoo, warehouse_id.lot_stock_id, lot_id=False, strict=False)
                    diferencia = stock_odoo - stock_woo
                    res = woo_report.create({
                        "product_id": product_odoo.id,
                        "store_product_id": mapping.store_product_id,
                        "cantidad_woo": stock_woo,
                        "cantidad_odoo": stock_odoo,
                        "diferencia": diferencia,
                        "state": "equal" if diferencia == 0 else "negative" if diferencia < 0 else "positive",
                        "warehouse_id": warehouse_id.id,
                        "stock_total_woo": stock_total_woo
                    })
                    if res:
                        self.env.cr.commit()

    def action_ajuste_stock(self):
        connection = self._get_woocommerce_connection()
        if not connection:
            return
        stock_quant = self.env['stock.quant']
        for record in self:
            URL_PATH = f'products/{record.store_product_id}'
            stock_quantity = stock_quant._get_available_quantity(
                record.product_id, record.warehouse_id.lot_stock_id, lot_id=False, strict=False)
            full_quantity = stock_quant.get_full_quantity_multi_warehouse(record.product_id)
            data = {
                "stock_quantity": full_quantity,
                "meta_data": [{
                    "key": record.warehouse_id.woo_branch,
                    "value": int(stock_quantity)
                }]
            }
            request_post = connection.post(
                URL_PATH,
                data
            )
            if request_post.ok and request_post.request.body:
                record.cantidad_odoo = stock_quantity
                record.cantidad_woo = stock_quantity
                record.diferencia = 0
                record.state = "updated"
                record.stock_total_woo = full_quantity
                self.env.cr.commit()

    def _get_woocommerce_connection(self):
        channel_id = self.env['multi.channel.sale'].sudo().search([
            ("channel", "=", "woocommerce"),
            ("state", "=", "validate")
        ], limit=1)
        if channel_id:
            req = API(
                url=channel_id.woocommerce_url,
                consumer_key=channel_id.woocommerce_consumer_key,
                consumer_secret=channel_id.woocommerce_secret_key,
                # wp_api=True,
                version="wc/v3",
                # query_string_auth=True,
                timeout=999,
            )
            return req
        else:
            _logger.info("No connection channel Woo available")
            return False

